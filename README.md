# @lpgroup/yup

Collection of yup validation functions and helper functions for projects based on feathersjs.

## Install

Installation of the npm

```sh
echo @lpgroup:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm install @lpgroup/yup
```

## Development

When developing this NPM your can patch yup to give better error message.

```sh
npm run patch-package
```

## Example

```javascript
const yup = require("@lpgroup/yup");
const schema = { key: yup.string().required() };
```

### Hooks

```javascript
/* xxx.yup.js */
const cy = require("@lpgroup/yup");

const requestSchema = {
  _id: cy.uuid().required(),
  alias: cy.string().required(),
  name: cy.string().required(),
  createdAt: cy.timestamp().defaultNull(),
  changedAt: cy.timestamp().defaultNull(),
};

const dbSchema = {
  added: cy.changed(),
  changed: cy.changed(),
  owner: cy.owner(),
};

module.exports = cy.buildValidationSchema(requestSchema, dbSchema);
module.exports.requestSchema = requestSchema;
```

```javascript
/* xxx.service.js */
const { Xxx } = require("./xxx.class");
const hooks = require("./xxx.hooks");
const schema = require("./xxx.yup");

module.exports = function (app) {
  const options = { id: "alias", schema };
  app.use("/xxx", new Xxx(options, app));
  const service = app.service("xxx");
  service.hooks(hooks);
};
```

```javascript
/* xxx.hooks.js */
const validateRequest = require("@lpgroup/yup").validateRequest;
const validateDatabase = require("@lpgroup/yup").validateDatabase;

module.exports = {
  before: {
    create: [validateRequest(), changeData(), validateDatabase()],
    update: [validateRequest(), changeData(), validateDatabase()],
    patch: [validateRequest(), changeData(), validateDatabase()],
  },
};
```

## Contribute

See https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md

## License

MIT, see LICENSE.MD

## API

### `general`

### `hooks`

#### `validateRequest(objSchema)`

```js
validateRequest(xxx);
```

#### `validateDatabase(objSchema)`

```js
validateDatabase(xxx);
```

#### `yup.array(objSchema)`

```js
const schema = { key: yup.array({ key: yup.string() }).required() };
```
